# start producer inside kafka container
kafka-console-producer.sh --broker-list localhost:9092 --topic lukb-topic

# start consumer inside (another) kafka container
kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic lukb-topic