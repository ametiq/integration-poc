package ch.lukb.integrationblueprint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntegrationBlueprintApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntegrationBlueprintApplication.class, args);
	}
}
