package ch.lukb.integrationblueprint.camel;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.http.common.HttpMethods;
import org.apache.http.entity.ContentType;
import org.springframework.stereotype.Component;

@Component
public class CamelRouteBuilder extends RouteBuilder {

    @Override
    public void configure() {
        log.info("Starting camel routes...");

        from("kafka:{{kafka.topic}}?" +
                "brokers={{kafka.host}}:{{kafka.port}}" +
                "&consumersCount={{kafka.consumer-count}}")
                .routeId("kafka-consumer-route")
                .log("Retrieved from Kafka: ${body}")
                .to("direct:call-api-route");

        from("direct:call-api-route")
                .routeId("call-api-route")
                .setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.GET.name()))
                .setHeader(Exchange.CONTENT_TYPE, constant(ContentType.APPLICATION_JSON.getMimeType()))
                .log("Calling remote API...")
                .to("{{lukb-demo.url}}")
                .unmarshal().json()
                .log("Result from API call:\n${body}")
                .to("direct:transform-json-route");

        from("direct:transform-json-route")
                .routeId("transform-json-route")
                .log("applying json transformation...")
                .to("jolt:jolt/schema.json")
                .to("direct:display-results-route");

        from("direct:display-results-route")
                .routeId("display-results-route")
                .log("Result is:\n${body}");
    }
}
