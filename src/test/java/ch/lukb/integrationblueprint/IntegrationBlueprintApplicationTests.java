package ch.lukb.integrationblueprint;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Disabled
class IntegrationBlueprintApplicationTests {

	@Test
	void contextLoads() {
	}

}
