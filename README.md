# Camel & Kafka integration PoC

## Goals
The goal of this PoC is to show the integration and transformation capabilities of different technologies
and how these can be combined, especially when kafka is used as the producer of data.

## Starting the demo
Requirements:  
- maven and java 11 (or downgrade to JDK 8, it should work)
- docker and docker-compose
- free ports on the host: 2181 and 9092
- access to internet (for api call)

Steps:  
1) ```mvn clean package```
2) ```cd demo && sh start_kafka.sh```
3) Login to running Kafka container: ```docker exec -u 0 -it demo_kafka_1 bash```
4) Start kafka producer: ```kafka-console-producer.sh --broker-list localhost:9092 --topic lukb-topic```
5) Start the spring-boot app
6) Enter some value on the container terminal where kafka producer is waiting on input
7) You should output from integration routes in the app's console
To stop the demo, use ```sh stop_kafka.sh``` and stop the spring-boot app.

## Technologies
- Camel
- Kafka
- Spring Boot
- Docker
- JOLT

### Author
Boris Gligorijevic
amétiq AG